package main;

import android.app.Application;

import alarm.AlarmProvider;
import service.FtpSenderService;

public class ApplicationProvider extends Application {

    @Override
    public void onCreate () {
        super.onCreate();
        AlarmProvider.startClockAlarm(this, FtpSenderService.class, FtpSenderService.SERVICE_ID);
    }
}
