package alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmProvider {
    private static final int CLOCK_REPEAT_TIME = 60000;

    public static void startClockAlarm(Context context, Class serviceClass,int serviceId) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, serviceClass);
        PendingIntent pi = PendingIntent.getService(context, serviceId, i, PendingIntent.FLAG_CANCEL_CURRENT);
        am.cancel(pi);
        am.setRepeating(AlarmManager.RTC, System.currentTimeMillis() ,CLOCK_REPEAT_TIME, pi);
    }
}
