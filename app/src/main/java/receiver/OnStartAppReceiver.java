package receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import service.FtpSenderService;
import alarm.AlarmProvider;

public class OnStartAppReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == Intent.ACTION_BOOT_COMPLETED) {
            AlarmProvider.startClockAlarm(context, FtpSenderService.class,FtpSenderService.SERVICE_ID);
        }
    }
}
