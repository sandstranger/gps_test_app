package service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import model.UserLocation;
import utils.Constants;
import utils.FileUtils;
import utils.GpsUtils;
import utils.LogPrinter;
import utils.ftp.FtpUtils;
import utils.ftp.IFileUploadingListener;

public class FtpSenderService extends Service implements IFileUploadingListener {
    public static final int SERVICE_ID = 78;
    private FtpUtils ftpUtils;
    @Override
    public void onCreate() {
        LogPrinter.printMessageToLogcat("SEND USER LOCATION DATA SERVICE STARTED");
        ftpUtils = new FtpUtils();
        ftpUtils.setFileUploadingListener(this);
        sendCurrentUserLocation();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void sendCurrentUserLocation() {
        Location userLocation = GpsUtils.getLastBestLocation(this);
        if (userLocation!=null) {
            String userLocationJsonFullPath = FileUtils.getFileFullPath(this, Constants.USER_LOCATION_JSON_FILENAME);
            UserLocation dataModel = new UserLocation();
            dataModel.setLatitude((float) userLocation.getLatitude());
            dataModel.setLongitude((float) userLocation.getLongitude());
            FileUtils.writeAllText(new Gson().toJson(dataModel), userLocationJsonFullPath);
            ftpUtils.uploadFileToServer(userLocationJsonFullPath);
        }
        else {
            LogPrinter.printMessageToLogcat("GPS POSITION IS EMPTY");
            stopSelf();
        }
    }

    @Override
    public void onFileUploadedSucsess() {
        LogPrinter.printMessageToLogcat("LOCATION DATA UPLOADED");
        stopSelf();
    }

    @Override
    public void onFileUploadedFailed() {
        LogPrinter.printMessageToLogcat("LOCATION DATA UPLOADED FAILED");
        stopSelf();
    }
}
