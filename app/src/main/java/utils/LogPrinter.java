package utils;

import android.util.Log;

import com.gps.test.BuildConfig;

public class LogPrinter {
    private static final String EXCEPTIOIN_TAG = "Exception";
    private static final String APP_TAG = "gpsapp";

    public static void printMessageToLogcat (String message){
        if (BuildConfig.DEBUG){
            Log.d(APP_TAG,message);
        }
    }

    public static void printExceptionToLogcat(Exception e){
        if (BuildConfig.DEBUG){
            Log.e(APP_TAG,EXCEPTIOIN_TAG,e);
        }
    }
}