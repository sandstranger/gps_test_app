package utils;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {


    public static void writeAllText(String text, String filePath) {
        try {
            File file = new File(filePath);
            org.apache.commons.io.FileUtils.writeStringToFile(file,text,false);
        } catch (Exception e) {
            LogPrinter.printExceptionToLogcat(e);
        }
    }

    public static String readAllText(String filePath) {
        String fileContent = "";
        File file = new File(filePath);
        if (file.exists()) {
            try {
                fileContent = org.apache.commons.io.FileUtils.readFileToString(file);
            } catch (IOException e) {
                LogPrinter.printExceptionToLogcat(e);
            }

        }
        return fileContent;
    }

    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    public static String getFileFullPath(Context context, String fileName) {
        return String.valueOf(context.getFilesDir() + "/" + fileName);
    }
}
