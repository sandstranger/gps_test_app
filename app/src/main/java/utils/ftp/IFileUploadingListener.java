package utils.ftp;

import android.content.Context;

public interface IFileUploadingListener {
    void onFileUploadedSucsess();

    void onFileUploadedFailed();
}
