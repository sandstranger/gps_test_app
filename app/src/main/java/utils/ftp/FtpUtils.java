package utils.ftp;

import android.os.AsyncTask;

import com.adeel.library.easyFTP;

import org.apache.commons.net.ftp.FTPClient;

import utils.FileUtils;
import utils.LogPrinter;

public class FtpUtils {
    private static final String FTP_SERVER_NAME = "f20-preview.royalwebhosting.net";
    private static final String FTP_USER_NAME = "2715445";
    private static final String FTP_USER_PASSWORD = "Pitrosyan156";
    private static final String FTP_WORKING_DIRECTORY = "/home/www";
    private UploadFileTask uploadFileAsyncTask;
    private DownloadFileTask downloadFileAsyncTask;
    private IFileUploadingListener fileUploadingListener;
    private IFileDownloadingListener fileDownloadingListener;

    public FtpUtils() {
    }

    public void setFileUploadingListener(IFileUploadingListener listener) {
        if (listener != null) {
            fileUploadingListener = listener;
        }
    }

    public void setFileDownloadingListener(IFileDownloadingListener listener) {
        if (listener != null) {
            fileDownloadingListener = listener;
        }
    }

    public void uploadFileToServer(String filePath) {
        stopUploadFileTask();
        uploadFileAsyncTask = new UploadFileTask();
        uploadFileAsyncTask.execute(filePath);
    }

    public void downloadFile(String fileName, String fullFilePath) {
        stopDownloadFileTask();
        downloadFileAsyncTask = new DownloadFileTask();
        downloadFileAsyncTask.execute(fileName, fullFilePath);
    }

    private void stopUploadFileTask() {
        if (uploadFileAsyncTask != null && uploadFileAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            uploadFileAsyncTask.cancel(true);
        }
    }

    private void stopDownloadFileTask() {
        if (downloadFileAsyncTask != null && downloadFileAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            downloadFileAsyncTask.cancel(true);
        }
    }


    private class UploadFileTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                String filePath = params[0];
                easyFTP ftp = buildEasyFTP();
                ftp.uploadFile(filePath);
                ftp.disconnect();
                return true;
            } catch (Exception e) {
                LogPrinter.printExceptionToLogcat(e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean fileUploaded) {
            if (fileUploadingListener == null) {
                return;
            }
            if (fileUploaded) {
                fileUploadingListener.onFileUploadedSucsess();
            } else {
                fileUploadingListener.onFileUploadedFailed();
            }
        }
    }

    private class DownloadFileTask extends AsyncTask<String, Void, Void> {
        String targetFilePath = "";

        @Override
        protected Void doInBackground(String... params) {
            try {
                String remoteFilePath = params[0];
                targetFilePath = params[1];
                easyFTP ftp = buildEasyFTP();
                ftp.downloadFile(remoteFilePath, targetFilePath);
                ftp.disconnect();
            } catch (Exception e) {
                LogPrinter.printExceptionToLogcat(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (fileDownloadingListener == null) {
                return;
            }
            String text = FileUtils.readAllText(targetFilePath);
            fileDownloadingListener.getFileDownloadingResult(text);
        }
    }

    private easyFTP buildEasyFTP() throws Exception {
        easyFTP ftp = new easyFTP();
        ftp.connect(FTP_SERVER_NAME, FTP_USER_NAME, FTP_USER_PASSWORD);
        FTPClient ftpClient = ftp.getFtpClient();
        ftpClient.setActivePortRange(50, 50);
        ftpClient.enterLocalPassiveMode();
        return ftp;
    }
}
