package utils.ftp;

public interface IFileDownloadingListener {
    void getFileDownloadingResult (String text);
}
