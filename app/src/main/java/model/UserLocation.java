package model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class UserLocation {
    @SerializedName("Latitude")
    private float latitude;
    @SerializedName("Longitude")
    private float longitude;

    public UserLocation(){

    }
}
